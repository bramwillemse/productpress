<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    
    <?php // START CONTENT BLOCKS ?>
    <?php while( has_sub_field('blocks') ): ?>

        <?php if(get_row_layout() == 'slides'): // layout: Slideshow ?>
            <?php get_template_part('_/inc/block-slides'); ?>

        <?php elseif(get_row_layout() == 'features'): // layout: Features ?>
            <?php get_template_part('_/inc/block-features'); ?>

        <?php elseif(get_row_layout() == 'videos'): // layout: Videos ?>
            <?php get_template_part('_/inc/block-videos'); ?>

        <?php elseif(get_row_layout() == 'form'): // layout: Form ?>
            <?php get_template_part('_/inc/block-form'); ?>

        <?php endif; ?> 
    
    <?php endwhile; ?>

<?php endwhile; else : ?>

    <h2 class="center">Niet gevonden</h2>
    <p class="center">Sorry, maar je zoekt naar een pagina die niet bestaan, probeer eens te zoeken rechtsboven op de site.</p>

<?php endif; ?> 
                           
<?php get_footer(); ?>