# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "/"
sass_dir = "_/css"
images_dir = "_/img"
javascripts_dir = "_/js"
fonts_dir = "_/fonts"

## Output: nested
output_style = :nested

## Output: minified
# output_style = :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false
color_output = false


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass _/css scss && rm -rf sass && mv scss sass
preferred_syntax = :scss
