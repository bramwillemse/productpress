	<footer class="l-footer footer">
		&copy; Copyright 2013 <a href="http://bramwillemse.nl" class="tk-lobster">De Bramster</a>
	</footer>

	<?php if ( get_field('company-phone', 'options') ) : ?>
		<div class="footer-mobile shadow-top">
			<a href="tel:<?php the_field('company-phone', 'options'); ?>" class="phone"><span class="ss-icon ss-standard">phone</span> <?php the_field('company-phone', 'options'); ?></a>

			<a href="#top" class="button button-top"><span class="ss-icon ss-standard">up</span> top</a>
		</div><!-- /.footer-mobile -->
	<?php endif; ?>

	<?php if ( function_exists( 'yoast_analytics' ) ) {
		yoast_analytics();
	} ?>

	<?php wp_footer(); ?>

</body>

</html> 