<?php
/*
* Template Name: Formulier 
*/ 
get_header(); ?>

<div class="l-content">
    <article id="post-<?php the_ID(); ?>" class="article article-form">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <?php get_template_part( '_/inc/header-article' ); ?>
        
            <div class="entry">
                <?php the_content('Lees verder &raquo;'); ?>
            </div><!-- /.entry -->

            <footer class="postmetadata">
            </footer><!-- /.postmetadata -->
                    

        <?php //get_template_part('_/inc/widgets-left'); ?>
    
    <?php endwhile; ?>

    <?php else : ?>

        <h2 class="center">Niet gevonden</h2>
        <p class="center">Sorry, maar je zoekt naar een pagina die niet bestaan, probeer eens te zoeken rechtsboven op de site.</p>

    <?php endif; ?>
    </article><!-- #article -->

</div><!-- /.l-content -->
                            
<?php get_footer(); ?>