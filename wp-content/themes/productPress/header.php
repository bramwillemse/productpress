<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- the "no-js" class is for Modernizr. -->

<head class="html5reset-kitchensink-commented">

	<!-- Typekit (async) -->
	<script type="text/javascript">
	  (function() {
	    var config = {
	      kitId: 'tzk6nmb',
	      scriptTimeout: 3000
	    };
	    var h=document.getElementsByTagName("html")[0];h.className+=" wf-loading";var t=setTimeout(function(){h.className=h.className.replace(/(\s|^)wf-loading(\s|$)/g," ");h.className+=" wf-inactive"},config.scriptTimeout);var tk=document.createElement("script"),d=false;tk.src='//use.typekit.net/'+config.kitId+'.js';tk.type="text/javascript";tk.async="true";tk.onload=tk.onreadystatechange=function(){var a=this.readyState;if(d||a&&a!="complete"&&a!="loaded")return;d=true;clearTimeout(t);try{Typekit.load(config)}catch(b){}};var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(tk,s)
	  })();
	</script><!-- /Typekit -->

	<meta charset="utf-8" >
	
	<!-- This prevents the conditional comments below from holding up the page load
		 www.phpied.com/conditional-comments-block-downloads/ -->
	<!--[if IE]><![endif]-->
			
	<meta name="author" content="Bram Willemse">
	<meta name="copyright" content="Copyright Bram Willemse 2013">

	<meta name="DC.title" content="<?php wp_title(''); ?>">
	<meta name="DC.subject" content="Best Product Page">
	<meta name="DC.creator" content="Bram Willemse, bramwillemse.nl">
        
    <?php if ( is_page() || is_single() ) : ?>    
	    <?php $image_id = get_post_thumbnail_id($post->ID);  
	    $image_url = wp_get_attachment_image_src($image_id,'large');  
	    $image_url = $image_url[0]; ?>
	
		<link rel="image_src" href="<?php echo $image_url; ?>" >
	<?php endif; ?>
    
	<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/_/img/favicon.png">
	<!--link rel="apple-touch-icon" href=""-->

	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    
    <meta property="og:title" content="<?php wp_title(); ?>">
    <meta property="og:site_name" content="<?php bloginfo('name'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <title><?php wp_title(''); ?></title>

    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>
</head>

<body>

	<header id="top" class="header header-main">
    	<h1 class="logo"><span><?php bloginfo('name'); ?></span></h1>

		<!-- Mobile toggle -->
    	<?php get_template_part('_/inc/button-toggle'); ?>

    	<div id="container-nav" class="container-nav">
	    	<!-- Main menu -->
	    	<?php get_template_part('_/inc/nav-main'); ?>

	    	<!-- Menu right (WordPress) -->
	    	<?php get_template_part('_/inc/nav-right'); ?>
	    	<!-- Language menu (WPML plugin) -->
	    	<?php //do_action('icl_language_selector'); ?>
	    </div>

	   	<!-- Mobile menu -->
		<?php get_template_part('_/inc/nav-mobile'); ?>
	   

	</header>