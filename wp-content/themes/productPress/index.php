<?php get_header(); ?>

<div class="l-container">              
    <div class="articles">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>            
        <!--div class="box-masonry"-->
        <article class="article">
            <?php if( has_post_thumbnail() ) : ?>
                <?php $thumbid = get_post_thumbnail_id($post->ID); ?>
                <?php $img = wp_get_attachment_image_src( $thumbid, 'news' ); ?>
                <?php $newsimg = $img[0]; ?>
                <a href="<?php the_permalink(); ?>" class="image image-news" style="background-image: url('<?php echo $newsimg; ?>');"></a>
            <?php endif; ?>              

            <header>
                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                <div class="postmetadata">
                    <span class="category"><?php $category = get_the_category(); echo $category[0]->cat_name;?></span> - 
                    <span class="date"><?php the_date(); ?></span>
                </div><!-- /.postmetadata -->
            </header>

            <div class="excerpt">
                <?php the_excerpt(); ?>
            </div>
        </article><!-- /.article -->
        <!-- /div><!- - /.box-masonry -->
    <?php endwhile; ?>

    <nav class="nav navigation">
        <?php html5wp_pagination(); ?>
    </nav>

    <?php else : ?>
    <article class="article">
        <h1>He bah!</h1>
        <p>Deze pagina bestaat niet. Het spijt me...</p>
    </article>
    <?php endif; ?> 
    </div><!-- /.articles -->

    <div class="widgets">
        <div class="widget">
            <h2>Widget</h2>
            <p>Widget content</p>
        </div>
    </div>
                            
</div><!-- /.l-content -->
                    
<?php get_footer(); ?>