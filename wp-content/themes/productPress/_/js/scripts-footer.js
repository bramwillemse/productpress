// remap jQuery to $
(function($) {
  $(document).ready(function($) {
    $('.flexslider').flexslider({
      animation: "fade",
      touch: true,
        useCSS: true,
        slideshowSpeed: 7000,
      animationSpeed: 500,
      directionNav: true
    });

    $('#button-toggle').click(function() {
      $('.nav-mobile').toggleClass('is-visible');
    });

    // Set sticky navigation
    $('#container-nav').waypoint('sticky', {
        wrapper: '<div class="l-sticky_wrapper"/>',
        stuckClass: 'is-stuck'
    });

    // Set nice scrolling for navigation
    $('.nav-main a[href*=#]').bind('click', function(e) {
      e.preventDefault();
      $(this).parent().parent().find('.is-active').removeClass('is-active');
      $(this).addClass('is-active');

      //prevent the "normal" behaviour which would be a "hard" jump
      var target = $(this).attr("href"); //Get the target
              
      // perform animated scrolling by getting top-position of target-element and set it as scroll target
      $('html, body').stop().animate({ scrollTop: $(target).offset().top - 28 }, 500, function() {
        location.hash = target;  //attach the hash (#jumptarget) to the pageurl
      });

      return false;
    });
    
  });
})(window.jQuery);