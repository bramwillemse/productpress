<div class="social">
	<h3>Volg AppFellas</h3>
    <?php if ( get_field('company-facebook','options') ) : ?>
	    <a href="http://facebook.com/<?php the_field('company-facebook','options'); ?>" target="_blank" class="button button-social button-facebook"><span class="ss-icon ss-social-regular">facebook</span> Facebook</a>
	<?php endif; ?>
    <?php if ( get_field('company-twitter','options') ) : ?>
	    <a href="http://twitter.com/<?php the_field('company-twitter','options'); ?>" target="_blank" class="button button-social button-twitter "><span class="ss-icon ss-social-regular">twitter</span> Twitter</a>
	<?php endif; ?>	    
    <?php if ( get_field('company-linkedin','options') ) : ?>
	    <a href="http://linkedin.com/company/<?php the_field('company-linkedin','options'); ?>" target="_blank" class="button button-social button-linkedin "><span class="ss-icon ss-social-regular">linkedin</span> Linkedin</a>
	<?php endif; ?>
    <?php if ( get_field('company-googleplus','options') ) : ?>
	    <a href="http://google.com/+/<?php the_field('company-googleplus','options'); ?>" target="_blank" class="button button-social button-googleplus"><span class="ss-icon ss-social-regular">googleplus</span> Google+</a>
	<?php endif; ?>
</div><!-- /.social -->