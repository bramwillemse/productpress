<div class="block block-news">
	<h2><?php the_sub_field('news-title'); ?></h2>

	<div class="l-container articles">
		<?php // If news selection is made, load selection of three articles
		$posts = get_sub_field('news-select');
		if( $posts ): ?>
			<?php // Set layout class based on total number of posts
			$count = count($posts);

			if( $count == 3 ) :
				$layoutClass = 'l-third';
			elseif( $count == 2 ) : 
				$layoutClass = 'l-half';
			elseif( $count == 1 ) :
				$layoutClass = 'l-fullwidth';
			endif; ?>

			<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
				<?php setup_postdata($post); ?>
			    <article class="<?php echo $layoutClass; ?> article">
			    	<header>
			            <?php if( has_post_thumbnail() ) : ?>
			               	<div class="image image-news">
				                <?php $thumbid = get_post_thumbnail_id($post->ID); ?>
				                <?php $img = wp_get_attachment_image_src( $thumbid, 'wide-small' ); ?>
				                <?php $newsimg = $img[0]; ?>
				                <a href="<?php the_permalink(); ?>"><img src="<?php echo $newsimg; ?>" class="l-centered"></a>
				            </div><!-- /.image-news -->
			            <?php endif; ?>

			    		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	                    <div class="postmetadata">
	                        <span class="date"><?php the_date(); ?></span>
	                    </div><!-- /.postmetadata -->
	                </header>

		            <div class="excerpt">  		    	
			            <?php the_excerpt(); ?>
	                    <a class="more-link" href="<?php the_permalink(); ?>">verder lezen <span class="ss-icon ss-standard">next</span></a>
			        </div><!-- /.excerpt -->
			    </article><!-- /.article -->		
			<?php endforeach; ?>
			<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

		<?php else : // If no selection has been made, load latest three articles ?>
			<?php $args = array(
				'post_type' => 'post',
				'showposts' => 3
			); ?>
			<?php $query = new WP_Query( $args ); ?>
			<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?> 
			    <article class="l-third article">
			    	<header>
			            <?php if( has_post_thumbnail() ) : ?>
			               	<div class="image image-news">
				                <?php $thumbid = get_post_thumbnail_id($post->ID); ?>
				                <?php $img = wp_get_attachment_image_src( $thumbid, 'wide-small' ); ?>
				                <?php $newsimg = $img[0]; ?>
				                <a href="<?php the_permalink(); ?>"><img src="<?php echo $newsimg; ?>" class="l-centered"></a>
				            </div><!-- /.image-header -->
			            <?php endif; ?>
			            
			    		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	                    <div class="postmetadata">
	                        <span class="date"><?php the_date(); ?></span>
	                    </div><!-- /.postmetadata -->			    		
			    	</header>

		            <?php the_excerpt(); ?>

                    <a class="more-link" href="<?php the_permalink(); ?>">verder lezen <span class="ss-icon ss-standard">next</span></a>

			    </article>					
			<?php endwhile; endif; ?>
			<?php /* Restore original Post Data */
			wp_reset_postdata(); ?>
		<?php endif;?>
	</div><!-- /.articles -->
</div><!-- /.block-news -->