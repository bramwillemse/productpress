<?php if( has_post_thumbnail() ) : ?>
    <?php $thumbid = get_post_thumbnail_id($post->ID); ?>
    <?php $img = wp_get_attachment_image_src( $thumbid, 'wide' ); ?>
    <?php $headerimg = $img[0]; ?>
<?php endif; ?>

<header class="header header-article">
    <h1><?php the_title(); ?></h1>

    <?php if( has_post_thumbnail() ) : ?>
	    <div class='image image-header' style="background-image: url('<?php echo $headerimg; ?>');">
		<?php if ( get_field('video-popup') ) : ?>
            <button id="play" class="button button-callout button-play"><span class="ss-icon ss-standard">play</span></button>
        <?php endif; ?>
        </div>
	<?php endif;?>

</header><!-- /.header-article -->

<?php if ( get_field('video-popup') ) : ?>
    <?php get_template_part('_/inc/popup-video'); ?>
<?php endif; ?>