<?php $defaults = array(
	'theme_location'  => 'nav-right',
	'menu'            => 'nav-right', 

	// disable container
	'container'       => '', 
	'container_class' => false, 
	'container_id'    => '',

	// set classes & id
	'menu_class'      => 'nav nav-right', 
	//'menu_id'         => '',

	'echo'            => true,
	'fallback_cb'     => 'wp_page_menu',
	'before'          => '',
	'after'           => '',
	'link_before'     => '',
	'link_after'      => '',
	'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
	'depth'           => 0,
	'walker'          => ''
); ?>

<?php wp_nav_menu( $defaults ); ?>