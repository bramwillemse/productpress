<?php if ( get_field('video-popup') ) : ?>
<?php $video = wp_oembed_get( get_field('video-popup') ); ?>
	<script>
		var videoPopup = '<?php echo '<div id="video" class="video video-popup">' . $video . '</div>'; ?>;'
	</script>

	<div id="popup" class="popup is-hidden">
		<button href="#" id="close" class="button button-close is-hidden"><span class="ss-icon ss-standard">close</span></button>		
	</div>

<?php endif; ?>
