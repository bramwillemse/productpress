<?php // Check if repeater is filled with content ?>
<?php if ( get_sub_field('select') ): ?>

    <?php // Generate slug
    if( get_sub_field('slug')) : ?>
        <?php $slug = get_sub_field('slug'); ?>
    <?php else : ?>
        <?php $slug = 'form'; ?>
    <?php endif; ?>

    <div id="<?php echo $slug; ?>" class="block form">
        <?php if ( get_sub_field('title')) : ?>
            <header class="header header-block">
                <h2 class="lined"><span><?php the_sub_field('title'); ?></span></h2>
            </header><!-- /.header-block -->
        <?php endif; ?>

        <div class="l-container">

			<?php if ( !get_sub_field('style') ) : 
				$formClass = 'l-twothirds';
				$textClass = 'l-third';
			else: 
				$formClass = 'l-fullwidth';
				$textClass = 'l-fullwidth';
			endif;?>


			<div class="<?php echo $formClass; ?> form">
				<?php if ( get_sub_field('select') ) : ?>
					<?php // Load selected form via gravity forms
				    $form = get_sub_field('select');
				    gravity_form_enqueue_scripts($form->id, true);
				    gravity_form($form->id, false, false, false, '', true, 1); ?>				
				<?php endif; ?>
			</div><!-- /.contactform -->

			<?php if ( get_sub_field('instructions') ) : ?>
	            <div class="<?php $textClass; ?> text instructions">
	                <?php if ( get_sub_field('instructions') ) : ?>
	                    <p>
	                        <?php the_sub_field('instructions'); ?>
	                    </p>
	                <?php endif; ?>
	            </div>
	        <?php endif; ?>

        </div><!-- /.l-container -->

    </div><!-- /.form -->

<?php endif; ?>   
