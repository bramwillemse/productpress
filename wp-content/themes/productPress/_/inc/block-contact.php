<div id="contact" class="block block-contact contact">
    <header class="header header-block">
        <h2 class="lined"><span>Contact</span></h2>
    </header><!-- /.header-block -->

	<div class="l-half contactform textalign-right">
	<?php if ( get_field('form-contact') ) : ?>
		<h3>Contact form</h3>
	    <?php $formid = get_field('form-contact'); ?>
	    <?php gravity_form_enqueue_scripts($formid, true); ?>
	    <?php gravity_form($formid, false, false, false, false, true, 1); ?>
	<?php endif; ?>
	</div><!-- /.contactform -->

	<div class="l-half contactdata textalign-left">
		<h3>Contact</h3>

		<p class="address">
		<?php // Street address ?>
		<?php if ( get_field('company-street','options') ) : ?>
			<?php the_field('company-street','options') ; ?><br/>
		<?php endif; ?>

		<?php // Postal code ?>
		<?php if ( get_field('company-postalcode','options') ) : ?>
			<?php the_field('company-postalcode','options') ; ?>
		<?php endif; ?>

		<?php // City ?>
		<?php if ( get_field('company-city','options') ) : ?>
			<?php the_field('company-city','options') ; ?><br/>
		<?php endif; ?>

		<?php // Country ?>
		<?php if ( get_field('company-country','options') ) : ?>
			<?php the_field('company-country','options') ; ?>
		<?php endif; ?>
		</p><!-- /.address -->

		<p class="phone">
		<?php // Phone number ?>
		<?php if ( get_field('company-phone','options') ) : ?>
			<a class="button" href="tel:<?php the_field('company-phone','options') ; ?>"><span class="ss-icon ss-standard">phone</span> +<?php the_field('company-phone','options') ; ?></a>
		<?php endif; ?>
		</p><!-- /.phone -->

		<p class="email">
		<?php // Email address ?>
		<?php if ( get_field('company-email','options') ) : ?>
			<a class="button" href="mailto:<?php the_field('company-email','options') ; ?>"><span class="ss-icon ss-standard">email</span> <?php the_field('company-email','options') ; ?></a>
		<?php endif; ?>
		</p><!-- /.email -->
	</div><!-- /.contactdata -->

	<?php get_template_part('_/inc/social'); ?>

</div>