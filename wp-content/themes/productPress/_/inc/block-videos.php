<?php // Check if repeater is filled with content ?>
<?php if(get_sub_field('select')): ?>
    <?php // Generate slug
    if( get_sub_field('slug')) : ?>
        <?php $slug = get_sub_field('slug'); ?>
    <?php else : ?>
        <?php $slug = 'video'; ?>
    <?php endif; ?>

	<div id="<?php echo $slug; ?>" class="block videos">
        <?php if ( get_sub_field('title')) : ?>
            <header class="header header-block">
                <h3 class="lined"><span><?php the_sub_field('title'); ?></span></h3>
            </header><!-- /.header-block -->
        <?php endif; ?>
	
        <div class="l-container">

            <?php // Load slides from relationship field
            $posts = get_sub_field('select');			
            if( $posts ) : 

            // Add right layout class based on total of posts
    		$countVideos = count($posts);
			if( $countVideos == 3 ) :
				$layoutClass = 'l-third';
			elseif( $countVideos == 2 ) : 
				$layoutClass = 'l-half';
			elseif( $countVideos == 1 ) :
				$layoutClass = 'l-fullwidth';
			endif; ?>

                <?php foreach( $posts as $post ) : // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
			
			        <?php if ( get_field('image') ) : ?>
						<?php $attachment_id = get_field('image'); ?>
				        <?php $videoBackground = wp_get_attachment_image_src( $attachment_id, 'large' ); ?>
				        <?php 
				        // url = $featureImage[0];
				        // width = $featureImage[1];
				        // height = $featureImage[2]; ?>
					<?php endif;?>

				    <div class="<?php echo $layoutClass; ?> video">
				        <h2><?php the_title(); ?></h2>

				        <?php // Load video
				        if ( get_field('url') ) : 
							echo wp_oembed_get( get_field('url') ); 
						endif; ?>

						<?php if (get_field('description') ) : ?>
							<p><?php the_field('description'); ?></p>
						<?php endif;?>
				    </div><!-- /.video -->
                <?php endforeach; ?>
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif; ?>
        </div><!-- /.l-container -->
	</div><!-- /.videos -->
<?php endif; ?>