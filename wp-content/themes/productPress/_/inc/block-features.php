<?php // Check if repeater is filled with content ?>
<?php if ( get_sub_field('select') ): ?>
    <?php // Generate slug
    if( get_sub_field('slug')) : ?>
        <?php $slug = get_sub_field('slug'); ?>
    <?php else : ?>
        <?php $slug = 'features'; ?>
    <?php endif; ?>

    <div id="<?php echo $slug; ?>" class="block features">
        <?php if ( get_sub_field('title')) : ?>
            <header class="header header-block">
                <h2 class="lined"><span><?php the_sub_field('title'); ?></span></h2>
            </header><!-- /.header-block -->
        <?php endif; ?>

        <div class="l-container">

            <?php // Load slides from relationship field
            $posts = get_sub_field('select');
            if( $posts ) : ?>

                <?php // Set layout class based on total number of posts
                $countFeatures = count($posts);

                if( $countFeatures == 3 ) :
                    $layoutClass = 'l-third';
                elseif( $countFeatures == 2 ) : 
                    $layoutClass = 'l-half';
                elseif( $countFeatures == 1 ) :
                    $layoutClass = 'l-fullwidth';
                endif; ?>

                <?php foreach( $posts as $post ) : // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>

                    <?php // Load slide image variables ?>
                    <?php if ( get_field('image') ) : ?>
                        <?php $attachment_id = get_field('image'); ?>
                        <?php $slideImage = wp_get_attachment_image_src( $attachment_id, 'large' ); ?>
                        <?php 
                        // url = $slideImage[0];
                        // width = $slideImage[1];
                        // height = $slideImage[2]; ?>
                    <?php endif;?>

                    <div class="<?php echo $layoutClass; ?> feature">
                        <?php if ( get_field('image') ) : ?>
                            <figure class="image image-adaptive" style="background-image: url(<?php echo $slideImage[0]; ?>);">
                            </figure>
                        <?php endif; ?>

                        <div class="text <?php //echo $textClass; ?>">
                            <h3><?php the_title(); ?></h3>
                            <?php if ( get_field('description') ) : ?>
                                <p class="description">
                                    <?php the_field('description'); ?>
                                </p>
                            <?php endif; ?>
                       </div>
 
                        <?php if ( get_field( 'buttons') ) : ?>
                            <ul class="buttons">              
                                <?php while(has_sub_field('buttons')): ?>
                                    <li>
                                        <a class="button" href="<?php the_sub_field('url'); ?>">
                                            <?php the_sub_field('label'); ?>
                                        </a>
                                    </li>
                                <?php endwhile; ?>
                            </ul>
                        <?php endif; ?>                             

            
                    </div>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif; ?>

        </div><!-- /.l-container -->

    </div><!-- /.features -->
<?php endif; ?>   
