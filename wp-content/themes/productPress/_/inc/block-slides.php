<?php if( get_sub_field('select') ): ?>
    <?php // Set id for navigation
    if( get_sub_field('slug')) { 
        $slug = get_sub_field('slug');
    } else {
        $slug = 'slideshow';
    } ?>

    <div id="<?php echo $slug; ?>" class="block flexslider">
        <?php if ( get_sub_field('title')) : ?>
            <header class="header header-block">
                <h2 class="lined"><span><?php the_sub_field('title'); ?></span></h2>
            </header><!-- /.header-block -->
        <?php endif; ?>
        
        <?php // Load slides from relationship field
        $posts = get_sub_field('select');
        if( $posts ) : ?>
            <ul id="<?php echo $slug; ?>" class="slides">
                <?php foreach( $posts as $post ) : // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>

                    <?php // Load slide image variables ?>
                    <?php if ( get_field('image') ) : ?>
                        <?php $attachment_id = get_field('image'); ?>
                        <?php $slideImage = wp_get_attachment_image_src( $attachment_id, 'large' ); ?>
                        <?php 
                        // url = $slideImage[0];
                        // width = $slideImage[1];
                        // height = $slideImage[2]; ?>
                    <?php endif;?>

                    <li class="slide">
                        <?php if ( get_field('description') ) : ?>
                            <div class="l-half l-centered is-left">
                                <div class="text text-slide">
                                    <h3><?php the_title(); ?></h3>
                                    <p class="description">
                                        <?php the_field('description'); ?>
                                    </p>
                                </div><!-- /.text-slide -->
                            </div><!-- /.l-half.l-centered -->
                        <?php endif; ?>                           

                        <?php if ( get_field('image') ) : ?>
                            <figure class="image image-adaptive image-slide ">
                                <img src="<?php echo $slideImage[0]; ?>" class="l-centered">
                            </figure>
                        <?php endif; ?>                 
                    </li>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            </ul><!-- /.slides -->
        <?php endif; ?>
    </div><!-- /.flexslider -->

<?php endif; ?> 
