<?php if ( get_sub_field('slide-image') ) : ?>
    <?php $attachment_id = get_sub_field('slide-image'); ?>
    <?php $slideImage = wp_get_attachment_image_src( $attachment_id, 'medium' ); ?>
    <?php 
    // url = $slideImage[0];
    // width = $slideImage[1];
    // height = $slideImage[2]; ?>
<?php endif;?>

<?php if ( get_sub_field('slide-background') ) :
$attachment_id = get_sub_field('slide-background');
$slideBackground = wp_get_attachment_image_src( $attachment_id, 'wide' );
// url = $slideBackground[0];
// width = $slideBackground[1];
// height = $slideBackground[2];
endif;?>

<div class="block block-slide" <?php if (get_sub_field('slide-background')) : ?>style="background-image: url(<?php echo $slideBackground[0]; ?>)"<?php endif;?> > 
    <div class="l-container">   
    <?php if ( get_sub_field('slide-image') ) : ?>
        <img class="image image-salesforce" src="<?php echo $slideImage[0]; ?>">
    <?php endif; ?>

        <div class="entry">
            <?php if ( get_sub_field('slide-title') ) : ?>
            <h2><?php the_sub_field('slide-title'); ?></h2>    
            <?php endif; ?>
            <?php if ( get_sub_field('slide-text') ) : ?>
            <p><?php the_sub_field('slide-text') ?></p>
            <?php endif;?>

            <?php if ( get_sub_field('button-normal-url') ) : ?>
            <a href="<?php the_sub_field('button-normal-url'); ?>" class="button"><?php the_sub_field( 'button-normal'); ?></a>
            <?php endif; ?>
            <?php if ( get_sub_field('button-slide-url') ) : ?>
            <a href="<?php the_sub_field('button-slide-url'); ?>" class="button button-slide"><?php the_sub_field( 'button-slide'); ?></a>
            <?php endif; ?>
        </div><!-- /.entry -->
    </div><!-- /.l-container -->
</div><!-- /.block-slide -->
