<?php // Convert block name to id ?>
<?php $blockLowercase = strtolower( get_sub_field('block-tweets-title') ); ?>
<?php $blockid = str_replace(' ', '-', $blockLowercase); ?>

<div id="<?php echo $blockid; ?>" class="block block-halves block-tweets">
	<div class="l-container">
			<?php /* Load sidebar to load Twitterfeed */
		    if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('widgets-twitter') ) : 
		    endif; ?>

			<a href="http://twitter.com/<?php the_field('company-twitter','options') ?>" class="more-link" target="_blank">meer tweets <span class="ss-icon ss-standard">&#x25BB;</span></a>

	</div><!-- /.l-container -->
</div><!-- /.block-halves -->