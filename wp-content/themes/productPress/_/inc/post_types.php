<?php 
	// Register Custom Post Type "Feature"
	function post_type_features() {
		$labels = array(
			'name'                => _x( 'Features', 'Post Type General Name', 'text_domain' ),
			'singular_name'       => _x( 'Feature', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'           => __( 'Features', 'text_domain' ),
			'parent_item_colon'   => __( 'Main feature', 'text_domain' ),
			'all_items'           => __( 'All features', 'text_domain' ),
			'view_item'           => __( 'View feature', 'text_domain' ),
			'add_new_item'        => __( 'Add new feature', 'text_domain' ),
			'add_new'             => __( 'Add new feature', 'text_domain' ),
			'edit_item'           => __( 'Edit feature', 'text_domain' ),
			'update_item'         => __( 'Update feature', 'text_domain' ),
			'search_items'        => __( 'Search features', 'text_domain' ),
			'not_found'           => __( 'No feature found', 'text_domain' ),
			'not_found_in_trash'  => __( 'No feature found in trash', 'text_domain' ),
		);

		$args = array(
			'label'               => __( 'feature', 'text_domain' ),
			'description'         => __( 'features-overzicht', 'text_domain' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'revisions' ),
			'taxonomies'          => false,
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'menu_icon'           => '',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);

		register_post_type( 'feature', $args );
	}
	// Hook into the 'init' action
	add_action( 'init', 'post_type_features', 0 );


	// Register Custom Post Type "Slide"
	function post_type_slides() {
		$labels = array(
			'name'                => _x( 'Slides', 'Post Type General Name', 'text_domain' ),
			'singular_name'       => _x( 'Slide', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'           => __( 'Slides', 'text_domain' ),
			'parent_item_colon'   => __( 'Main slide', 'text_domain' ),
			'all_items'           => __( 'All slides', 'text_domain' ),
			'view_item'           => __( 'View slide', 'text_domain' ),
			'add_new_item'        => __( 'Add new slide', 'text_domain' ),
			'add_new'             => __( 'Add new slide', 'text_domain' ),
			'edit_item'           => __( 'Edit slide', 'text_domain' ),
			'update_item'         => __( 'Update slide', 'text_domain' ),
			'search_items'        => __( 'Search slides', 'text_domain' ),
			'not_found'           => __( 'No slides found', 'text_domain' ),
			'not_found_in_trash'  => __( 'No slides found in trash', 'text_domain' ),
		);

		$args = array(
			'label'               => __( 'slide', 'text_domain' ),
			'description'         => __( 'slides-overzicht', 'text_domain' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'revisions' ),
			'taxonomies'          => false,
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 6,
			'menu_icon'           => '',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);

		register_post_type( 'slide', $args );
	}
	// Hook into the 'init' action
	add_action( 'init', 'post_type_slides', 0 );


	// Register Custom Post Type "App stores"
	function post_type_affiliates() {
		$labels = array(
			'name'                => _x( 'Affiliates', 'Post Type General Name', 'text_domain' ),
			'singular_name'       => _x( 'Affiliate', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'           => __( 'Affiliates', 'text_domain' ),
			'parent_item_colon'   => __( 'Main Affiliate', 'text_domain' ),
			'all_items'           => __( 'All Affiliates', 'text_domain' ),
			'view_item'           => __( 'View Affiliate', 'text_domain' ),
			'add_new_item'        => __( 'Add new Affiliate', 'text_domain' ),
			'add_new'             => __( 'Add new Affiliate', 'text_domain' ),
			'edit_item'           => __( 'Edit Affiliate', 'text_domain' ),
			'update_item'         => __( 'Update Affiliate', 'text_domain' ),
			'search_items'        => __( 'Search Affiliates', 'text_domain' ),
			'not_found'           => __( 'No Affiliates found', 'text_domain' ),
			'not_found_in_trash'  => __( 'No Affiliates found in trash', 'text_domain' ),
		);

		$args = array(
			'label'               => __( 'affiliate', 'text_domain' ),
			'description'         => __( 'affiliates-overzicht', 'text_domain' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'revisions' ),
			'taxonomies'          => false,
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 7,
			'menu_icon'           => '',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);

		register_post_type( 'affiliate', $args );
	}
	// Hook into the 'init' action
	add_action( 'init', 'post_type_affiliates', 0 );


	// Register Custom Post Type "Video"
	function post_type_videos() {
		$labels = array(
			'name'                => _x( 'Videos', 'Post Type General Name', 'text_domain' ),
			'singular_name'       => _x( 'Video', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'           => __( 'Videos', 'text_domain' ),
			'parent_item_colon'   => __( 'Main Video', 'text_domain' ),
			'all_items'           => __( 'All Videos', 'text_domain' ),
			'view_item'           => __( 'View Video', 'text_domain' ),
			'add_new_item'        => __( 'Add new video', 'text_domain' ),
			'add_new'             => __( 'Add new video', 'text_domain' ),
			'edit_item'           => __( 'Edit Video', 'text_domain' ),
			'update_item'         => __( 'Update Video', 'text_domain' ),
			'search_items'        => __( 'Search Videos', 'text_domain' ),
			'not_found'           => __( 'No Videos found', 'text_domain' ),
			'not_found_in_trash'  => __( 'No Videos found in trash', 'text_domain' ),
		);

		$args = array(
			'label'               => __( 'video', 'text_domain' ),
			'description'         => __( 'videos-overzicht', 'text_domain' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'revisions' ),
			'taxonomies'          => false,
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 8,
			'menu_icon'           => '',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);

		register_post_type( 'video', $args );
	}
	// Hook into the 'init' action
	add_action( 'init', 'post_type_videos', 0 );


	// Add icons to Post types
	function post_type_icons() { ?>
	    <style type="text/css" media="screen">
	        /* Post type: Feature */
	        #menu-posts-feature .wp-menu-image {
	            background: url(<?php bloginfo('template_url') ?>/_/img/icon-feature.png) no-repeat 0px -33px !important;
	        }

	        #menu-posts-feature:hover .wp-menu-image, 
	        #menu-posts-feature.wp-has-current-submenu .wp-menu-image {
	            background-position: 0 -1px!important;
	        }

	        /* Post type: Feature */
	        #menu-posts-slide .wp-menu-image {
	            background: url(<?php bloginfo('template_url') ?>/_/img/icon-feature.png) no-repeat 0px -33px !important;
	        }

	        #menu-posts-slide:hover .wp-menu-image, 
	        #menu-posts-slide.wp-has-current-submenu .wp-menu-image {
	            background-position: 0 -1px!important;
	        }

	       	/* Post Type: Video */
	        #menu-posts-video .wp-menu-image{
	            background: url(<?php bloginfo('template_url') ?>/_/img/icon-video.png) no-repeat 0px -33px !important;
	        }
	        #menu-posts-video:hover .wp-menu-image,
	        #menu-posts-video.wp-has-current-submenu .wp-menu-image {
	            background-position: 0 -1px!important;
	        }

	       	/* Post Type: Affiliate */
	        #menu-posts-affiliate .wp-menu-image{
	            background: url(<?php bloginfo('template_url') ?>/_/img/icon-feature.png) no-repeat 0px -33px !important;
	        }
	        #menu-posts-affiliate:hover .wp-menu-image,
	        #menu-posts-affiliate.wp-has-current-submenu .wp-menu-image {
	            background-position: 0 -1px!important;
	        }

	    </style><?php 
	} 
	add_action( 'admin_head', 'post_type_icons' );
?>