<?php // Check if repeater is filled with content ?>
<?php if ( get_sub_field('externals') ): ?>
    <?php if( get_sub_field('externals-slug')) : ?>
        <?php $slug = get_sub_field('externals-slug'); ?>
    <?php else : ?>
        <?php $slug = 'externals'; ?>
    <?php endif; ?>

    <div id="<?php echo $slug; ?>" class="block block-light externals">
        <?php if ( get_sub_field('externals-title')) : ?>
            <header class="header header-block">
                <h2 class="lined"><span><?php the_sub_field('externals-title'); ?></span></h2>
            </header><!-- /.header-block -->
        <?php endif; ?>        

        <?php  // Loop through rows ?>
        <?php while( has_sub_field('externals') ): ?>

            <?php // Load external image variables ?>
            <?php if ( get_sub_field('external-image') ) : ?>
                <?php $attachment_id = get_sub_field('external-image'); ?>
                <?php $externalImage = wp_get_attachment_image_src( $attachment_id, 'medium' ); ?>
                <?php 
                // url = $externalImage[0];
                // width = $externalImage[1];
                // height = $externalImage[2]; ?>
            <?php endif;?>

            <div class="l-third external">
                <?php if ( get_sub_field('external-image') ) : ?>
                    <img src="<?php echo $externalImage[0]; ?>" class="image image-fullwidth">
                <?php endif; ?>
                <?php if ( get_sub_field('external-title') ) : ?>
                    <h3><?php the_sub_field('external-title'); ?></h3> 
                <?php endif; ?> 
                <?php if ( get_sub_field('external-text') ) : ?>
                    <p><?php the_sub_field('external-text'); ?></p> 
                <?php endif; ?> 
                <?php if ( get_sub_field('button-url')) : ?>
                    <a href="<?php the_sub_field('button-url'); ?>" class="button button-callout"><?php the_sub_field('button-label'); ?></button></a>
                <?php elseif (get_sub_field('button-attachment')) : ?>
                    <a href="<?php the_sub_field('button-attachment'); ?>" class="button button-callout"><?php the_sub_field('button-label'); ?></button></a>
                <?php endif;?>
            </div><!-- /.external -->

        <?php endwhile; ?>
    </div><!-- /.externals -->
<?php endif; ?>   
