<div class="block block-social">
	<h2>Volg ons</h2>
    <?php if ( get_field('company-facebook', 'options')) : ?>
	    <a href="http://facebook.com/<?php the_field('company-facebook','options'); ?>" target="_blank" class="button button-facebook button-transparent"><span class="ss-icon ss-social">facebook</span> Facebook</a>
	<?php endif ?>
    <?php if ( get_field('company-twitter', 'options')) : ?>	
    <a href="http://twitter.com/<?php the_field('company-twitter','options'); ?>" target="_blank" class="button button-twitter button-transparent"><span class="ss-icon ss-social">twitter</span> Twitter</a>
	<?php endif ?>
    <?php if ( get_field('company-linkedin', 'options')) : ?>
        <a href="http://linkedin.com/company/<?php the_field('company-linkedin','options'); ?>" target="_blank" class="button button-linkedin button-transparent"><span class="ss-icon ss-social">linkedin</span> Linkedin</a>
	<?php endif ?>
	<?php if ( get_field('company-googleplus', 'options')) : ?>
        <a href="http://plus.google.com/<?php the_field('company-googleplus','options'); ?>" target="_blank" class="button button-googleplus button-transparent"><span class="ss-icon ss-social">googleplus</span> Google+</a>
	<?php endif ?>
</div>