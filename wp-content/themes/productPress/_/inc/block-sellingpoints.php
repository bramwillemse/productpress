<?php // Check if repeater is filled with content ?>
<?php if ( get_sub_field('sellingpoints') ): ?>
    <?php if( get_sub_field('sellingpoints-slug')) : ?>
        <?php $slug = get_sub_field('sellingpoints-slug'); ?>
    <?php else : ?>
        <?php $slug = 'sellingpoints'; ?>
    <?php endif; ?>

    <div id="<?php echo $slug; ?>" class="block block-light sellingpoints" style="background-image: url(<?php echo $sellingpointsBackground[0]; ?>);">
        <?php if ( get_sub_field('sellingpoints-title')) : ?>
            <header class="header header-block">
                <h2 class="lined"><span><?php the_sub_field('sellingpoints-title'); ?></span></h2>
            </header><!-- /.header-block -->
        <?php endif; ?>

        <?php  // Loop through rows ?>
        <?php while( has_sub_field('sellingpoints') ): ?>
            <?php // Load feature image variables ?>
            <?php if ( get_sub_field('sellingpoint-image') ) : ?>
                <?php $attachment_id = get_sub_field('sellingpoint-image'); ?>
                <?php $sellingpointImage = wp_get_attachment_image_src( $attachment_id, 'normal' ); ?>
                <?php 
                // url = $sellingpointImage[0];
                // width = $sellingpointImage[1];
                // height = $sellingpointImage[2]; ?>
            <?php endif;?>

            <div class="sellingpoint l-third">
                <?php if ( get_sub_field('sellingpoint-image') ) : ?>
                    <img src="<?php echo $sellingpointImage[0]; ?>">
                <?php endif; ?>
                <?php if ( get_sub_field('sellingpoint-title') ) : ?>
                    <h3><?php the_sub_field('sellingpoint-title'); ?></h3> 
                <?php endif;?> 
                <?php if ( get_sub_field('sellingpoint-text') ) : ?>
                    <p><?php the_sub_field('sellingpoint-text'); ?></p> 
                <?php endif;?> 
            </div><!-- /.sellingpoint -->

        <?php endwhile; ?>
    </div><!-- /.sellingpoints -->
<?php endif; ?>   
