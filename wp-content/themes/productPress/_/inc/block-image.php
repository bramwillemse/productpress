<div class="block block-image">
    <?php $attachment_id = get_sub_field('image'); ?>
	<?php $attachment_url = wp_get_attachment_image_src( $attachment_id, 'wide' ); ?>
	<div class="image image-header" style="background-image: url('<?php echo $attachment_url[0]; ?>');">
	</div>
</div><!-- /.block-image -->