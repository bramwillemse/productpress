<?php
/* 	=============================================================================
   	WordPress backend additions
   	========================================================================== */

	if ( function_exists( 'add_theme_support' ) ) { 
		// Add support for custom menu's
		add_theme_support('menus');
		register_nav_menus( array(
			'nav-main' => 'main navigation',
			'nav-right' => 'navigation right',
		) );	
	}

	// Allow more HTML tags in the editor
	function fb_change_mce_options($initArray) {
		$ext = 'pre[id|name|class|style],iframe[align|longdesc| name|width|height|frameborder|scrolling|marginheight| marginwidth|src]';
	
		if ( isset( $initArray['extended_valid_elements'] ) ) {
			$initArray['extended_valid_elements'] .= ',' . $ext;
		} else {
			$initArray['extended_valid_elements'] = $ext;
		}
	
		return $initArray;
	}
	add_filter('tiny_mce_before_init', 'fb_change_mce_options');

	// customize admin footer text
	// function custom_admin_footer() {
	//         echo '<a href="http://bramwillemse.nl/" target="_blank">Bram Willemse</a> was here';
	// } 
	// add_filter('admin_footer_text', 'custom_admin_footer');	

	// Include widget "Contact"
	include_once( rtrim( dirname( __FILE__ ), '/' ) . '/_/inc/widget-contact.php' );

	/**
	 * Simple wrapper for native get_template_part()
	 * Allows you to pass in an array of parts and output them in your theme
	 * e.g. <?php get_template_parts(array('part-1', 'part-2')); ?>
	 *
	 * @param 	array 
	 * @return 	void
	 * @author 	Keir Whitaker
	 **/
	function get_template_parts( $parts = array() ) {
		foreach( $parts as $part ) {
			get_template_part( $part );
		};
	}

	/* Load custom Admin CSS  */
	// function load_custom_wp_admin_style() {
	//         wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/admin-style.css', false, null );
	//         wp_enqueue_style( 'custom_wp_admin_css' );
	// }
	// add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );


	// Rearrange the admin menu
	function custom_menu_order($menu_ord) {
		if (!$menu_ord) return true;
		return array(
			'index.php', // Dashboard
			'edit.php?post_type=page', // Pages
			'separator1', // First separator			
		);
	}
	add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
	add_filter('menu_order', 'custom_menu_order');

	function edit_admin_menus() {  
	    global $menu;  
	      
	    $menu[10][0] = 'Images &amp; Docs'; // Change Media to Images
	}  
	add_action( 'admin_menu', 'edit_admin_menus' );  


/* 	=============================================================================
   	WordPress backend removals
   	========================================================================== */

	// remove unnecessary header info
	function remove_header_info() {
	    remove_action('wp_head', 'rsd_link');
	    remove_action('wp_head', 'wlwmanifest_link');
	    remove_action('wp_head', 'wp_generator');
	    remove_action('wp_head', 'start_post_rel_link');
	    remove_action('wp_head', 'index_rel_link');
	    remove_action('wp_head', 'adjacent_posts_rel_link');         // for WordPress <  3.0
	    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head'); // for WordPress >= 3.0
	}
	add_action('init', 'remove_header_info');

	// Remove items from admin menu
	function remove_admin_bar_items() {
	        global $wp_admin_bar;
	       
	        $wp_admin_bar->remove_menu('wp-logo'); /* Remove WordPress Logo */
	        $wp_admin_bar->remove_menu('comments'); /* Remove 'Add New > Posts' */
	        $wp_admin_bar->remove_menu('new-post'); /* Remove 'Add New > Posts' */
	}
	add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_items', 0 ); // Remove items from admin bar

	// Remove items in admin menu
	function remove_admin_menu_items() {
		// Remove 'Comments'
		remove_menu_page('edit-comments.php');

		// Remove submenu item: 'Appearance > Customize'
		remove_submenu_page('themes.php', 'customize.php');

		// Conditional removals 
		if(!current_user_can('edit_themes')) { // Remove items for editors and below
			remove_menu_page('tools.php'); 
		}
		
	}
	add_action('admin_menu', 'remove_admin_menu_items');

   	// Deactivate dashboard widgets
	function my_custom_dashboard_widgets() {
		global $wp_meta_boxes;
		 //Right Now - Comments, Posts, Pages at a glance
		// unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
		//Recent Comments
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
		//Incoming Links
		// unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
		//Plugins - Popular, New and Recently updated Wordpress Plugins
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);

		//Wordpress Development Blog Feed
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
		//Other Wordpress News Feed
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
		//Quick Press Form
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
		//Recent Drafts List
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
	}
	add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');


	// remove extra CSS that 'Recent Comments' widget injects
	function remove_recent_comments_style() {
	    global $wp_widget_factory;
	    remove_action('wp_head', array(
	        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
	        'recent_comments_style'
	    ));
	}
	add_action('widgets_init', 'remove_recent_comments_style');	

	// Disable RSS feeds
	function fb_disable_feed() {
		wp_die( __('No feed available,please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
	}
	add_action('do_feed', 'fb_disable_feed', 1);
	add_action('do_feed_rdf', 'fb_disable_feed', 1);
	add_action('do_feed_rss', 'fb_disable_feed', 1);
	add_action('do_feed_rss2', 'fb_disable_feed', 1);
	add_action('do_feed_atom', 'fb_disable_feed', 1);


/* 	=============================================================================
   	Disable comments 
   	========================================================================== */

	// Disable support for comments and trackbacks in post types
	function df_disable_comments_post_types_support() {
		$post_types = get_post_types();
		foreach ($post_types as $post_type) {
			if(post_type_supports($post_type, 'comments')) {
				remove_post_type_support($post_type, 'comments');
				remove_post_type_support($post_type, 'trackbacks');
			}
		}
	}
	add_action('admin_init', 'df_disable_comments_post_types_support');

	// Close comments on the front-end
	function df_disable_comments_status() {
		return false;
	}
	add_filter('comments_open', 'df_disable_comments_status', 20, 2);
	add_filter('pings_open', 'df_disable_comments_status', 20, 2);

	// Hide existing comments
	function df_disable_comments_hide_existing_comments($comments) {
		$comments = array();
		return $comments;
	}
	add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);

	// Redirect any user trying to access comments page
	function df_disable_comments_admin_menu_redirect() {
		global $pagenow;
		if ($pagenow === 'edit-comments.php') {
			wp_redirect(admin_url()); exit;
		}
	}
	add_action('admin_init', 'df_disable_comments_admin_menu_redirect');

	// Remove comments metabox from dashboard
	function df_disable_comments_dashboard() {
		remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
	}
	add_action('admin_init', 'df_disable_comments_dashboard');

	// Remove comments links from admin bar
	function df_disable_comments_admin_bar() {
		if (is_admin_bar_showing()) {
			remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
		}
	}
	add_action('init', 'df_disable_comments_admin_bar');


/* 	=============================================================================
   	WordPress backend navigation
   	========================================================================== */

	// Remove items admin menu
	function remove_menus () {
	global $menu;
		$restricted = array(
			// __('Dashboard'),
			__('Posts'), 
			// __('Media'), 
			// __('Links'), 
			// __('Pages'), 
			// __('Appearance'), 
			// __('Tools'), 
			// __('Users'), 
			// __('Settings'),
			__('Comments')
			// __('Plugins')
		);
		end ($menu);
		while (prev($menu)){
			$value = explode(' ',$menu[key($menu)][0]);
			if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
		}
	}
	add_action('admin_menu', 'remove_menus');

	// Remove items admin submenu
	function adjust_the_wp_menu() {
		$page = remove_submenu_page( 'themes.php', 'widgets.php' );
		// $page[0] is the menu title
		// $page[1] is the minimum level or capability required
		// $page[2] is the URL to the item's file
	}
	add_action( 'admin_menu', 'adjust_the_wp_menu', 999 );



/* 	=============================================================================
   	WordPress frontend
   	========================================================================== */

	// Protocol relative URLs for enqueued scripts/styles
	// function html5blank_protocol_relative($url) {
	// 	if(is_admin()) return $url;
	// 	return str_replace(array('http:','https:'), '', $url, $c=1);
	// }
	// add_filter('script_loader_src', 'html5blank_protocol_relative'); // Protocol relative URLs for enqueued scripts


	// Remove the <div> surrounding the dynamic navigation to cleanup markup
	function my_wp_nav_menu_args( $args = '' ) {
		$args['container'] = false;
		return $args;
	}

	// Remove Injected classes, ID's and Page ID's from Navigation <li> items
	function my_css_attributes_filter($var) {
		return is_array($var) ? array() : '';
	}

	// Remove invalid rel attribute values in the categorylist
	function remove_category_rel_from_category_list($thelist){
	     return str_replace('rel="category tag"', 'rel="tag"', $thelist);
	}
	
	// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
	function add_slug_to_body_class( $classes ) {
		global $post;
		if( is_home() ) {			
			$key = array_search( 'blog', $classes );
			if($key > -1) {
				unset( $classes[$key] );
			};
		} elseif( is_page() ) {
			$classes[] = sanitize_html_class( $post->post_name );
		} elseif(is_singular()) {
			$classes[] = sanitize_html_class( $post->post_name );
		};

		return $classes;
	}

	// remove more link
	function remove_more_jump_link($link) { 
		$offset = strpos($link, '#more-');
		if ($offset) {
			$end = strpos($link, '"',$offset);
		}
		if ($end) {
			$link = substr_replace($link, '', $offset, $end-$offset);
		}
		return $link;
	}
	
	add_filter('the_content_more_link', 'remove_more_jump_link');

	// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
	function html5wp_pagination() {
		global $wp_query;
		$big = 999999999;
		echo paginate_links( array(
			'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages )
		);
	}

	// Custom Excerpts
	function html5wp_index($length) { // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
	    return 33;
	}
	function html5wp_custom_post($length) { // Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
	    return 40;
	}

	// Create the Custom Excerpts callback
	function html5wp_excerpt($length_callback='', $more_callback='') {
	    global $post;
	    if(function_exists($length_callback)){
	        add_filter('excerpt_length', $length_callback);
	    }
	    if(function_exists($more_callback)){
	        add_filter('excerpt_more', $more_callback);
	    }
	    $output = get_the_excerpt();
	    $output = apply_filters('wptexturize', $output);
	    $output = apply_filters('convert_chars', $output);
	    $output = '<p>'.$output.'</p>';
	    echo $output;
	}
		
	// Custom View Article link to Post
	function html5wp_view_article($more) {
		global $post;
		return ' ...';
	}
	
	// Remove 'text/css' from our enqueued stylesheet
	function html5_style_remove($tag) {
		return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
	}


/* 	=============================================================================
	Images
	===========================================================================*/

	if ( function_exists( 'add_theme_support' ) ) { 
		// Add support for Post Thumbnails
		// add_theme_support( 'post-thumbnails' ); 
		add_theme_support( 'post-thumbnails', array( 'post', 'video','feature' ) );

	}

	// Add image size: Header image for pages and singles
	// if ( function_exists( 'add_image_size' ) ) { 
	// 	add_image_size( 'wide', 1280, 720, true ); // Article header image
	// }
	
	// If Dynamic Sidebar Exists
	// if(function_exists('register_sidebar')) {
	// 	register_sidebar(array(
	// 		'name' => 'Over mij',
	// 		'id' => 'widgets-about',
	// 		'class' => 'widgets-about',
	// 		'before_widget' => '<aside class="column column-small widget widget-left">',
	// 		'after_widget' => '</aside>',
	// 		'before_title' => '<h2>',
	// 		'after_title' => '</h2>',
	// 	));
	// }

	// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
	function remove_thumbnail_dimensions( $html ){
	    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
	    return $html;
	}

	// Allow SVG upload
	function cc_mime_types( $mimes ){
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}

	// Load functions
	add_filter( 'upload_mimes', 'cc_mime_types' );


/* 	=============================================================================
	Security
	===========================================================================*/

	// Remove WordPress version number
	//remove_action(‘wp_head’, ‘wp_generator’);


/* 	=============================================================================
	Plugins
	===========================================================================*/

	// Custom Styles plugin
	include_once( rtrim( dirname( __FILE__ ), '/' ) . '/_/plugins/custom_styles.php' );


	add_filter("gform_init_scripts_footer", "init_scripts");
	function init_scripts(){
		    return true;
	}

/* 	=============================================================================
   	Stylesheets
   	========================================================================== */

	// Theme Stylesheets using Enqueue
	function html5blank_styles() {
		wp_register_style( 'html5blank', get_template_directory_uri() . '/style.css', array(), '0.1', 'all');
		wp_enqueue_style( 'html5blank' ); // Enqueue it!
	}

	// Load functions
   	add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet	


/* 	=============================================================================
   	Scripts
   	========================================================================== */

	// Load HTML5 Blank scripts (header.php)
	function html5blank_header_scripts() {
	    if (!is_admin()) {
			wp_enqueue_script('jquery'); // Reregister WordPress jQuery in footer

			// registers script, stylesheet local path, no dependency, no version, loads in header
	        wp_register_script('headerscripts', get_stylesheet_directory_uri() . '/_/js/scripts-header-ck.js', false, null, false ); // Header scripts
	        wp_enqueue_script('headerscripts'); // Enqueue it!
	    }
	}

	// Load HTML5 Blank scripts (footer.php)
	function html5blank_footer_scripts() {
	    if (!is_admin()) {
			// wp_deregister_script('jquery'); // Deregister WordPress jQuery
	        
	        wp_register_script('footerscripts', get_template_directory_uri() . '/_/js/scripts-footer-ck.js', array(), null); // Main scripts
	        wp_enqueue_script('footerscripts'); // Enqueue it!
	    }
	}

	// Load functions
	add_action( 'init', 'html5blank_header_scripts' ); // Add Custom Scripts to wp_head
	add_action( 'wp_footer', 'html5blank_footer_scripts' ); // Add Custom Scripts to wp_footer
		
	
/* 	=============================================================================
   	Users, roles & capabilities
   	========================================================================== */  

	// Add certain admin roles to editor
	$_the_roles = new WP_Roles();
	$_the_roles->add_cap('editor','list_users');
	// $_the_roles->add_cap('editor','edit_users');
	// $_the_roles->add_cap('editor','create_users');
	$_the_roles->add_cap('editor','delete_users');
	$_the_roles->add_cap('editor','edit_theme_options');

	// Add & Remove contact information fields
	function ps_dm_user_contactmethods($user_contactmethods){
		$user_contactmethods['phone'] = 'Telefoon';
		unset($user_contactmethods['yim']);
		unset($user_contactmethods['aim']);
		unset($user_contactmethods['jabber']);
		$user_contactmethods['twitter'] = 'Twitter';
		$user_contactmethods['facebook'] = 'Facebook';
		$user_contactmethods['linkedin'] = 'Linkedin';
		$user_contactmethods['user_title'] = 'Website Name';
		$user_contactmethods['functie'] = 'Functie';

		//$user_contactmethods['gplus'] = 'Google Plus';
		return $user_contactmethods;
	}
	// Use this code to embed in template:
	// echo get_user_meta(1, 'twitter', true);


/* 	=============================================================================
   	Oembed settings / add-ons
   	========================================================================== */  

	// Add Twitter support
	function twitter_oembed($a) {
	    $a['#http(s)?://(www\.)?twitter.com/.+?/status(es)?/.*#i'] = array( 'http://api.twitter.com/1/statuses/oembed.{format}', true);
	    return $a;
	}
			
	// Add container to video's
	function your_theme_embed_filter( $output, $data, $url ) {
		if ( $data->type == 'video' ) {
			$return = '<figure class="box box-video">'.$output.'</figure>';
			return $return;
		}
		if ( $data->provider_name == 'Twitter' ) {
			$return = '<figure class="box box-tweet">'.$output.'</figure>';
			return $return;		
			//echo print_r($data);
		}
	}

	function modify_youtube_embed_url($html) {
		if( get_field('video-popup') ) {
			$autoplay = '&autoplay=1';
		}
	    return str_replace("?feature=oembed", "?feature=oembed&". $autoplay ."vq=hd720&showinfo=0&autohide=1", $html);
	}

	// Load Oembed functions
	add_filter('oembed_providers','twitter_oembed');	
	add_filter('oembed_dataparse', 'your_theme_embed_filter', 90, 3 );
	add_filter('oembed_result', 'modify_youtube_embed_url');


/* 	=============================================================================
   	Advanced Custom Fields (plugin)
   	========================================================================== */  

   	// Register multiple options pages
	// if(function_exists("register_options_page")) 
	// {
	//     // register_options_page('General Options');
	//     register_options_page('Company Details');
	// }
	// if( function_exists('acf_set_options_page_title') ) 
	// {
	// 	acf_set_options_page_title( 'Page Settings' );	
	// }

	// Create an advanced sub page called 'Footer' that sits under the General options menu
	if( function_exists('acf_add_options_sub_page') )
	{
	    acf_add_options_sub_page(array(
	        'title' => 'Company details',
	        'parent' => 'options-general.php',
	        'capability' => 'publish_pages'
	    ));
	}


	// function my_register_fields() 
	// {
	// 	include_once('_/plugins/acf-gravity_forms.php');
	//     include_once('_/plugins/advanced-custom-fields-nav-menu-field/fz-acf-nav-menu.php');
	// }	
	// add_action('acf/register_fields', 'my_register_fields'); // Load ACF Functions


/* =============================================================================
   Actions + Filters + ShortCodes
   ========================================================================== */

	// Add Actions
	add_action( 'init', 'html5wp_pagination' ); // Add our HTML5 Pagination
	add_action( 'login_head', 'custom_login_logo' ); // Add custom logo to login screen

	// Remove Actions
	remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
	remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
	remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
	remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
	remove_action( 'wp_head', 'index_rel_link' ); // index link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
	remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
	remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head',10, 0 );
	remove_action( 'wp_head', 'rel_canonical');
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
	
	// Add Filters
	add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
	add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
	add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
	add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
	add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
	add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
	add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
	add_filter('excerpt_more', 'html5wp_view_article'); // Add 'View Article' button instead of [...] for Excerpts
	add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
	add_filter( 'user_contactmethods', 'ps_dm_user_contactmethods', 10, 1); // Add & remove certain contact information fields from user profile


/* 	=============================================================================
   	Custom Post Types
   	========================================================================== */  
   	
	include_once('_/inc/post_types.php');

?>